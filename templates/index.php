<?php require_once '_header.php' ?>

    <div class="banner" style="background-color: #00b3ee; height: 300px;"></div>

    <div class="index-rating">
        <div class="head">
            <div><img src="/dist/img/icon_rating.png" alt=""></div>
            Рейтинг
            <div class="small">Апрель</div>
        </div>
        <div class="content">
            <div class="table">
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">1. Салик</div>
                        <div class="score">165</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">4. Fater</div>
                        <div class="score">135</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">7. Фейдман</div>
                        <div class="score">119</div>
                    </div>
                </div>
            </div>
            <div class="table">
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">2. Ослик</div>
                        <div class="score">144</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">5. Каблук</div>
                        <div class="score">127</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">8. Патыррр</div>
                        <div class="score">107</div>
                    </div>
                </div>
            </div>
            <div class="table">
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">3. Нургазы</div>
                        <div class="score">137</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">6. Фанат</div>
                        <div class="score">122</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">9. Пупок</div>
                        <div class="score">94</div>
                    </div>
                </div>
            </div>
            <div class="button">
                <a href="#">Весь список</a>
            </div>
        </div>
    </div>


    <div class="index-block">
        <div class="head">
            <img src="/dist/img/thumbnail-1.jpg" alt="">
        </div>
        <div class="content">
            <div class="table-block">
                <div class="table-cell table-date">
                    26 apr
                    <div class="year">2016</div>
                </div>
                <div class="table-cell">
                   День 2 - Знакомство с пластилином (Play-Doh First Time Motion)
                </div>
            </div>
            <div class="text">
                5 вопросов - 25 баллов максимум
            </div>
            <div class="button">
                <a href="./quiz-info.php">Подробнее ...</a>
            </div>
        </div>
    </div>

    <div class="index-block">
        <div class="head">
            <img src="/dist/img/thumbnail-1.jpg" alt="">
        </div>
        <div class="content">
            <div class="table-block">
                <div class="table-cell table-date">
                    26 apr
                    <div class="year">2016</div>
                </div>
                <div class="table-cell">
                    День 2 - Знакомство с пластилином (Play-Doh First Time Motion)
                </div>
            </div>
            <div class="text">
                5 вопросов - 25 баллов максимум
            </div>
            <div class="button">
                <a href="./quiz-info.php">Подробнее ...</a>
            </div>
        </div>
    </div>

    <div class="index-block">
        <div class="head">
            <img src="/dist/img/thumbnail-1.jpg" alt="">
        </div>
        <div class="content">
            <div class="table-block">
                <div class="table-cell table-date">
                    26 apr
                    <div class="year">2016</div>
                </div>
                <div class="table-cell">
                    День 2 - Знакомство с пластилином (Play-Doh First Time Motion)
                </div>
            </div>
            <div class="text">
                5 вопросов - 25 баллов максимум
            </div>
            <div class="button">
                <a href="./quiz-info.php">Подробнее ...</a>
            </div>
        </div>
    </div>

<?php require_once '_footer.php' ?>