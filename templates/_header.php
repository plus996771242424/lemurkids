<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Neucha&subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Scada:400,700&subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="/dist/css/app.css" />
    <title>Document</title>
</head>
<body>

<div class="site-container">
    <div class="header">
        <div class="element">
            <a href="./"><img src="/dist/img/logo.png" alt=""></a>
        </div>
        <div class="element">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Поиск по сайту">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
            </div>
        </div>
        <div class="element center">
            <a href="#"><img src="/dist/img/btn_youtube.png" alt=""></a>
        </div>
        <div class="element right">
            <button type="button" class="btn btn-primary">Войти</button>
        </div>
    </div>
    <div class="main-menu">
        <a href="./">Главная</a>
        <a href="contacts.php">О проекте</a>
        <a href="quiz.php">Опросы</a>
        <a href="rewards.php">Награда</a>
        <a href="#">Наш канал</a>
    </div>

