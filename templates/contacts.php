<?php require_once '_header.php' ?>

    <div class="contacts">
        <h2>О проекте</h2>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus doloribus ipsa necessitatibus veniam vero? Doloremque excepturi minima minus perferendis quas quidem tempore voluptatum? Deserunt eligendi facere fugit porro ratione soluta. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolores eveniet, inventore modi molestias reprehenderit? Aliquam aperiam cum eum excepturi inventore iure laborum magni minima obcaecati, saepe ut voluptas, voluptatum? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci architecto culpa cumque debitis dicta dolor, dolorem doloremque error ex, facere impedit ipsa necessitatibus quia quos ratione repudiandae tempore vel!</p>

        <h2 class="underline">Наши каналы на YouTube</h2>

        <div class="row">
            <div class="col-sm-4">
                <img src="../dist/img/thumbnail-1.jpg" class="img-responsive">
            </div>
            <div class="col-sm-8">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, corporis deleniti enim est facere laudantium nemo nobis odio omnis pariatur qui quo, ratione sapiente voluptas voluptatum! Commodi eaque illo nesciunt? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi delectus eaque eligendi enim, exercitationem hic illum ipsa labore magnam maiores quas quibusdam quod quos reiciendis reprehenderit tempore veritatis vitae.
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <img src="../dist/img/thumbnail-1.jpg" class="img-responsive">
            </div>
            <div class="col-sm-8">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, corporis deleniti enim est facere laudantium nemo nobis odio omnis pariatur qui quo, ratione sapiente voluptas voluptatum! Commodi eaque illo nesciunt? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi delectus eaque eligendi enim, exercitationem hic illum ipsa labore magnam maiores quas quibusdam quod quos reiciendis reprehenderit tempore veritatis vitae.
            </div>
        </div>

        <h2 class="underline">Наши группы в соц сетях</h2>

        <div class="row">
            <div class="col-sm-3"><img src="../dist/img/thumbnail-1.jpg" class="img-responsive"></div>
            <div class="col-sm-3"><img src="../dist/img/thumbnail-1.jpg" class="img-responsive"></div>
            <div class="col-sm-3"><img src="../dist/img/thumbnail-1.jpg" class="img-responsive"></div>
            <div class="col-sm-3"><img src="../dist/img/thumbnail-1.jpg" class="img-responsive"></div>
        </div>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio esse eveniet numquam porro quasi qui quo rem repellat vitae voluptates! Commodi cupiditate deleniti dolore, excepturi expedita optio rem rerum ut?</p>
    </div>

<?php require_once '_footer.php' ?>