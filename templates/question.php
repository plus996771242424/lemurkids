<?php require_once '_header.php' ?>

    <h2>Викторниа - Some question</h2>

    <div class="question-title-block">
        <div class="table-cell table-date">
            26 apr
            <div class="year">2016</div>
        </div>
        <div class="table-cell">
            День 2 - Знакомство с пластилином (Play-Doh First Time Motion)
        </div>
    </div>

    <div class="row question-progress">
        <div class="col-sm-4 text-right">
            Ты завершил
        </div>
        <div class="col-sm-4">
            <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                    <span class="sr-only">40% Complete (success)</span>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <span class="color">5</span> из 9
        </div>
    </div>

    <div class="question-block">
        <div class="text1">
            Вопрос 5. <span class="red">Неправильно!</span>
        </div>
        <div class="text2">
            Когда человек может быть в комнате без головы?
        </div>
        <div class="content">
            <div class="cell">
                <img src="../dist/img/thumbnail-1.jpg" alt="">
            </div>
            <div class="cell">
                <p><input type="radio"> Отказаться от гражданства</p>
                <p><input type="radio"> Пройти опрос и покланиться</p>
                <p><input type="radio"> Молча прогуляться по набережной</p>
                <p><input type="radio"> Неделю мыть посуду</p>
                <p><input type="radio"> Разбить соседу стекло</p>
            </div>
        </div>
        <div class="button">
            <a href="#">Следующий вопрос</a>
        </div>
    </div>

    <div class="question-block">
        <div class="text1">
            Вопрос 5.
        </div>
        <div class="text2">
            Когда человек может быть в комнате без головы?
        </div>
        <div class="content">
            <div class="cell">
                <textarea class="form-control" rows="5"></textarea>
            </div>
        </div>
        <div class="button">
            <a href="#">Следующий вопрос</a>
        </div>
    </div>

    <div class="question-block">
        <div class="text1">
            Вопрос 5. <span class="green">Правильно!</span>
        </div>
        <div class="text2">
            Когда человек может быть в комнате без головы?
        </div>
        <div class="content">
            <div class="cell">
                <p><input type="radio"> Отказаться от гражданства</p>
                <p><input type="radio"> Пройти опрос и покланиться</p>
                <p><input type="radio"> Молча прогуляться по набережной</p>
                <p><input type="radio"> Неделю мыть посуду</p>
                <p><input type="radio"> Разбить соседу стекло</p>
            </div>
        </div>
        <div class="button">
            <a href="#">Следующий вопрос</a>
        </div>
    </div>

<?php require_once '_footer.php' ?>