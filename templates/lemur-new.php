<br><br>
<div class="row"> 
		<h3 class="lemur-month"><span class="lemur-month-md"><?=$quiz['quiz_day'].' '.mee_date('M',$quiz['month']);  ?></span></br><span class="lemur-item-year"><?=$quiz['quiz_year'];?></span></h3>
		<h2 class="lemur-item-title"><? echo $quiz['title'];?></h2>
</div>
<div class="panel panel-default">
		<div class="panel-heading">
				<div class="panel-title"><b>Новая работа</b></div> 
		</div>
		<div class="panel-body">
				<form novalidate="" action="./lemur-new.php?quiz=<?=$_GET['quiz'];?>" enctype="multipart/form-data" method="post" role="form" class="form-horizontal">
						<div class="imageOutput p-20 thumbnail"><? echo '<img class="img-rounded" src="'.$lemur['image'].'">'; ?></div>
<? if($quiz){echo '<div style="display:none;"><input hidden="hidden" type="number" name="quiz_id" value="'.$quiz['quiz_id'].'"></div>';}  ?>
<? if($lemur){echo '<div style="display:none;"><input hidden="hidden" type="number" name="lemur_id" value="'.$lemur['lemur_id'].'"></div>';}  ?>
  					<div class="form-group">
								<label class="col-md-4 control-label">Загрузите Вашу работу</label> <div class="col-md-8"><input type="file" name="image"  class="form-control imageUpload" multiple="true" /></div>
						</div>   
						<div class="form-group">
								<label class="col-md-4 control-label">Тема</label>
								<div class="col-md-8"><input type="text" value="<? if($lemur){echo $lemur['title'];}?>" placeholder="Тема" id="title" class="form-control" name="title"></div>
						</div> 
						<div class="form-group">
								<label class="col-md-4 control-label" for="description">Описание</label>
								<div class="col-md-8">
										<textarea required="" class="form-control" placeholder="Описание" rows="10" cols="30" id="description" name="description"><? if($lemur){echo $lemur['description'];}?></textarea>
								</div>
						</div>
						<div class="form-group"> 
								<div class=" col-md-offset-4 col-md-8">
										<button class="btn btn-info" type="submit">Отправить</button>
								</div>
						</div>
				</form>
		</div>
</div>
<script>
$images = $('div.imageOutput');
$(".imageUpload").change(function(event){ $("div.imageOutput").empty(); readURL(this); });
function readURL(input) { 
		if(input.files && input.files[0]) {
				$.each(input.files, function() {
						var reader = new FileReader();
						reader.onload = function (e) {           
								$images.append('<img class="img-rounded" src="'+ e.target.result+'" />')
						}
				reader.readAsDataURL(this);
				});
		}
}
</script>