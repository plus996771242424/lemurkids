<?php require_once '_header.php' ?>

    <div class="index-block">
        <div class="head">
            <img src="/dist/img/thumbnail-1.jpg" alt="">
        </div>
        <div class="content">
            <div class="table-block">
                <div class="table-cell table-date">
                    26 apr
                    <div class="year">2016</div>
                </div>
                <div class="table-cell">
                    День 2 - Знакомство с пластилином (Play-Doh First Time Motion)
                </div>
            </div>
            <div class="text">
                5 вопросов - 25 баллов максимум
            </div>
            <div class="text-red">
                <div class="red-title">Правила прохождения опроса:</div>
                <ul>
                    <li>Отказаться от гражданства</li>
                    <li>Пройти опрос и покланиться</li>
                    <li>Молча прогуляться по набережной</li>
                    <li>Неделю мыть посуду</li>
                    <li>Разбить соседу стекло</li>
                </ul>
            </div>
            <div class="button-red">
                <a href="question.php">Начать ...</a>
            </div>
        </div>
    </div>

<?php require_once '_footer.php' ?>