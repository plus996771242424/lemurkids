<?php require_once '_header.php' ?>

    <h1>Награды</h1>

    <div class="index-rating">
        <div class="head">
            <div><img src="/dist/img/icon_rating.png" alt=""></div>
            Рейтинг
            <div class="small">Апрель</div>
        </div>
        <div class="content">
            <div class="table">
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">1. Салик</div>
                        <div class="score">165</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">4. Fater</div>
                        <div class="score">135</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">7. Фейдман</div>
                        <div class="score">119</div>
                    </div>
                </div>
            </div>
            <div class="table">
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">2. Ослик</div>
                        <div class="score">144</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">5. Каблук</div>
                        <div class="score">127</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">8. Патыррр</div>
                        <div class="score">107</div>
                    </div>
                </div>
            </div>
            <div class="table">
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">3. Нургазы</div>
                        <div class="score">137</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">6. Фанат</div>
                        <div class="score">122</div>
                    </div>
                </div>
                <div class="cell">
                    <div class="ranktable">
                        <div class="text">9. Пупок</div>
                        <div class="score">94</div>
                    </div>
                </div>
            </div>
            <div class="button">
                <a href="#">Весь список</a>
            </div>
        </div>
    </div>

    <h1>Призовой фонд</h1>

    <div class="rewards-prize">
        <div class="table">
            <div class="cell">1 место</div>
            <div class="cell"><img src="/dist/img/thumbnail-1.jpg" alt=""></div>
            <div class="cell">Мяч-прыгун</div>
        </div>
        <div class="table">
            <div class="cell">2 место</div>
            <div class="cell"><img src="/dist/img/thumbnail-1.jpg" alt=""></div>
            <div class="cell">Тачка на прокачку</div>
        </div>
    </div>

    <h1>История выдачи призов</h1>

    <div class="pagination">
        <div class="cel1">
            <a href="#">1</a>
            <span>...</span>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#" class="active">7</a>
            <a href="#">8</a>
            <a href="#">9</a>
            <span>...</span>
            <a href="#">28</a>
        </div>
        <div class="cel2">
            На странице:
            <a href="#" class="active">20</a>
            <a href="#">40</a>
            <a href="#">80</a>
        </div>
    </div>

    <div class="rewards-winners">
        <div class="title">Февраль 2016</div>
        <div class="table">
            <div class="cel1"><img src="/dist/img/thumbnail-1.jpg" alt=""></div>
            <div class="cel2">
                Место: 1 <br/>
                Победитель: Адилет <br/>
                Приз: Стиральная машина <br/>
            </div>
        </div>
        <div class="table">
            <div class="cel1"><img src="/dist/img/thumbnail-1.jpg" alt=""></div>
            <div class="cel2">
                Место: 2 <br/>
                Победитель: Светлана <br/>
                Приз: Гончарный аппарат <br/>
            </div>
        </div>
    </div>

    <div class="pagination">
        <div class="cel1">
            <a href="#">1</a>
            <span>...</span>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#" class="active">7</a>
            <a href="#">8</a>
            <a href="#">9</a>
            <span>...</span>
            <a href="#">28</a>
        </div>
        <div class="cel2">
            На странице:
            <a href="#" class="active">20</a>
            <a href="#">40</a>
            <a href="#">80</a>
        </div>
    </div>

<?php require_once '_footer.php' ?>