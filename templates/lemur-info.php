<div class="panel lemur-item   ">
    <div class="row"> 
        <div class="thumbnail">
            <img class="img-rounded" src="<?=$lemur['image'];?>"> 
        </div>  
        <h2 class="lemur-item-title"><?=$lemur['title'];?></h2>
        <div class="content"><?=$lemur['description'];?></div> 
    </div> 
</div> 
<br><br>